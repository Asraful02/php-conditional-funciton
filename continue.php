<?php
$arr = array(1, 2, 3);
foreach($arr as $number) {
  if($number == 2) {
    continue;
  }
  print $number;
}

echo"<br>";


$stack = array('first', 'second', 'third', 'fourth', 'fifth'); 

foreach($stack AS $v){ 
    if($v == 'second')continue; 
    if($v == 'fourth')break; 
    echo $v.'<br>'; 
} 

echo"<br>";

function square($num)
{
    return $num * $num;
}
echo square(4); 

?>